from django.http import HttpResponse
from django.shortcuts import render
from django.shortcuts import get_object_or_404
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from .models import Detail


def welcome(request):
    return HttpResponse("Welcome User")

class Detail(APIView):

    def get(self, request):
        stocks = Detail.objects.all()
        serializer = DetailSerializer(stocks, many=True)
        return Response(serializer.data)

    def post(self):
        pass


