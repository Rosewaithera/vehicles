from rest_framework import serializers
from .models import Detail

class StockSerializer(serializers.ModelSerializer):

    class Meta:
        model = Detail
        #fields = ('manufacturer', 'make')
        fields = '__all__'