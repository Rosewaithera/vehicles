from __future__ import unicode_literals
from django.db import models


class Question(models.Model):
    question_text = models.CharField(max_length=200)
    pub_date = models.DateTimeField('date published')


class Detail(models.Model):
    manufacturer_text = models.CharField(max_length=200)
    make_text = models.CharField(max_length=200)
    model_text = models.CharField(max_length=200)
    price_varchar = models.CharField(max_length=200)


class Choice(models.Model):
    question = models.ForeignKey(Question)
    choice_text = models.CharField(max_length=200)
    votes = models.IntegerField(default=0)


class Information(models.Model):
  detail = models.ForeignKey(Detail)
  make_text = models.CharField(max_length=200)
  model_text = models.CharField(max_length=200)
  price_varchar = models.CharField(max_length=200)
  choice_text = models.CharField(max_length=200)
